# Pi-hole curated blocklists

Thank you to everyone who has helped contribute to these blocklists for Pi-Hole. We couldn't do it without the help of the community!

While I do my best to keep these lists updated and curated, please note that largely it depends on what technologies you are using on your internal network(s). This comprehensive list aims to protect against most of the commonly found "default on" trackers. 

To update your Pi-hole, from the Dashboard select `Group Management`, then `Adlists`. From here you can bulk upload the .txt list in the repository. 
